//
//  ServiceAppDelegate.swift
//  CuAround
//
//  Created by Rizwan on 10/31/19.
//  Copyright © 2019 iOSApp. All rights reserved.
//

import UIKit

public class ServiceAppDelegate: NSObject, UIApplicationDelegate {
    public static let shared = ServiceAppDelegate()
    private override init() { }
    
    public func applicationDidFinishLaunching(application: UIApplication, launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
        customizeAppNavigationBar()
    }
    
    func customizeAppNavigationBar() {
        UIDevice.current.isBatteryMonitoringEnabled = true
        UINavigationBar.appearance().barTintColor = UIColor.appThemeColor()
        let navigationFontAttribute = [NSAttributedString.Key.font: UIFont.appDemiBoldFontWithSize(size: 20),NSAttributedString.Key.foregroundColor: UIColor.appBaseColor()]
        UINavigationBar.appearance().titleTextAttributes = navigationFontAttribute
        UINavigationBar.appearance().largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.appBaseColor()]
        UIBarButtonItem.appearance().setTitleTextAttributes(navigationFontAttribute, for: .normal)
        UINavigationBar.appearance().layer.borderColor = UIColor.appTintColor().cgColor
        UINavigationBar.appearance().layer.shadowColor = UIColor.appTransparencyBG().cgColor
        UINavigationBar.appearance().layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        UINavigationBar.appearance().layer.shadowRadius = 3.0
        UINavigationBar.appearance().layer.shadowOpacity = 1.0
        UINavigationBar.appearance().layer.masksToBounds =  false
        UINavigationBar.appearance().tintColor = UIColor.appTintColor()
        UIBarButtonItem.appearance().tintColor = UIColor.appTintColor()
        UINavigationBar.appearance().isTranslucent = false
    }
}
