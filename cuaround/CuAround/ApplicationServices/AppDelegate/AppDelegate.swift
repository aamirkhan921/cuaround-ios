//
//  AppDelegate.swift
//  CuAround
//
//  Created by Rizwan on 10/17/19.
//  Copyright © 2019 iOSApp. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FBSDKLoginKit
import FBSDKCoreKit
import PGSideMenu
import GoogleSignIn
import GoogleMaps
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        ServiceAppDelegate.shared.applicationDidFinishLaunching(application: application, launchOptions: launchOptions)
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        IQKeyboardManager.shared.enable = true
        GIDSignIn.sharedInstance()?.clientID = googleOAuthKey
        GMSServices.provideAPIKey(googleMapApiKey)
        //loadExampleAppStructure()
//        Localizer.DoTheSwizzling()
        FirebaseApp.configure()
        return true
    }

    fileprivate func loadExampleAppStructure() {
        let sideMenuController = PGSideMenu(animationType: .slideInRotate)
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.makeKeyAndVisible()
         let contentController = LogInViewController.initiateFromAppStoryBoard(appStoryBoard: .Main)
         let leftMenuController = SideMenuViewController.initiateFromAppStoryBoard(appStoryBoard: .Main)
        sideMenuController.addContentController(contentController)
        sideMenuController.addLeftMenuController(leftMenuController)
        self.window?.rootViewController = sideMenuController
    }
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        let fbHandle = ApplicationDelegate.shared.application(app, open: url, options: options)
        let googleHandle = (GIDSignIn.sharedInstance()?.handle((url)))!
        return fbHandle || googleHandle
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
      return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
      
    }

}

