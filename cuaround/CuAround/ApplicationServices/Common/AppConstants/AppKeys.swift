//
//  AppKeys.swift
//  CuAround
//
//  Created by Rizwan on 10/20/19.
//  Copyright © 2019 iOSApp. All rights reserved.
//

let defaultAppCountryCode = "KW";
let defaultAppCountryName = "Kuwait";
let defaultAppCountryCallingCode = "+965";
let defaultAppCountryCurrencyCode = "KWD"
let passwordLengthCount = 6
let nameLengthCount = 6
let appAlphaUrl                = "https://api.cuaround.app/_api/"// "http://104.211.215.136:2351/_api/"//
let appBetaUrl                 = "http://www.futuristic5.com/Andro-App-42/_api/service/"
let appLiveUrl                 = "https://api.cuaround.app/"

let googleOAuthKey = "939102463805-u97dm5tpb28us3ijm6to1i7ct288h37g.apps.googleusercontent.com"
let googleSigninApi = "AIzaSyDqa-H7D5QbChoEWwXtJuJCHaXEfeZHFj0"
let googleMapApiKey = "AIzaSyATFSXO520qR8jp9r9T4UtNej8DMLOaFEI"// "AIzaSyBoHXIHyvPezCupPmgo5QawmbUXiU5bhP0"//"AIzaSyBvvPJNawndvQL3j1-EEieQn_lT2vc5sz4"//
let appTwitterApiKey = "mRPiiPDk6G65NuBPunSLMY8UT"
let appTwitterSecretKey = "8VpUoJk6bSbCBLe19KzK3yHuuTJbUMYuNZhJJh9X3kuBdVkcaZ"
private let alphaGoogleMapsApiKey = ""
private let alphaGoogleMapsBrowserKey = ""

private let betaGoogleMapsApiKey = "AIzaSyATFSXO520qR8jp9r9T4UtNej8DMLOaFEI"
private let betaGoogleMapsBrowserKey = ""

private let liveGoogleMapsApiKey = "AIzaSyATFSXO520qR8jp9r9T4UtNej8DMLOaFEI"
private let liveGoogleMapsBrowserKey = "";

let appStoreURL              = ""

#if DEBUG
public let appApiBaseUrl = appAlphaUrl
public let appGoogleMapsApiKey = googleMapApiKey
public let appGoogleMapsBrowserKey = alphaGoogleMapsBrowserKey
#elseif BETA
public let appApiBaseUrl = appBetaUrl
public let appGoogleMapsApiKey = betaGoogleMapsApiKey
public let appGoogleMapsBrowserKey = betaGoogleMapsBrowserKey
#elseif LIVE
public let appApiBaseUrl = appLiveUrl
public let appGoogleMapsApiKey = liveGoogleMapsApiKey
public let appGoogleMapsBrowserKey = liveGoogleMapsBrowserKey
#endif
