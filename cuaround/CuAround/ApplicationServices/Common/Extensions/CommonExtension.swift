//
//  CommonExtension.swift
//  CuAround
//
//  Created by Rizwan on 10/20/19.
//  Copyright © 2019 iOSApp. All rights reserved.
//

import UIKit
extension UIDevice {
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        switch identifier {
        case "iPod5,1": return "iPod Touch 5"
        case "iPod7,1": return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3": return "iPhone 4"
        case "iPhone4,1": return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2": return "iPhone 5"
        case "iPhone5,3", "iPhone5,4": return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2": return "iPhone 5s"
        case "iPhone7,2": return "iPhone 6"
        case "iPhone7,1": return "iPhone 6 Plus"
        case "iPhone8,1": return "iPhone 6s"
        case "iPhone8,2": return "iPhone 6s Plus"
        case "iPhone9,1", "iPhone9,3": return "iPhone 7"
        case "iPhone9,2", "iPhone9,4": return "iPhone 7 Plus"
        case "iPhone8,4": return "iPhone SE"
        case "iPhone10,1", "iPhone10,4": return "iPhone 8"
        case "iPhone10,2", "iPhone10,5": return "iPhone 8 Plus"
        case "iPhone10,3", "iPhone10,6": return "iPhone X"
        case "iPhone11,2": return "iPhone XS"
        case "iPhone11,4", "iPhone11,6": return "iPhone XS Max"
        case "iPhone11,8": return "iPhone XR"
        case "iPhone12,1": return "iPhone 11"
        case "iPhone12,3": return "iPhone 11 Pro"
        case "iPhone12,5": return "iPhone 11 Pro Max"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4": return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3": return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6": return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3": return "iPad Air"
        case "iPad5,3", "iPad5,4": return "iPad Air 2"
        case "iPad6,11", "iPad6,12": return "iPad 5"
        case "iPad2,5", "iPad2,6", "iPad2,7": return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6": return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9": return "iPad Mini 3"
        case "iPad5,1", "iPad5,2": return "iPad Mini 4"
        case "iPad6,3", "iPad6,4": return "iPad Pro 9.7 Inch"
        case "iPad6,7", "iPad6,8": return "iPad Pro 12.9 Inch"
        case "iPad7,1", "iPad7,2": return "iPad Pro 12.9 Inch 2. Generation"
        case "iPad7,3", "iPad7,4": return "iPad Pro 10.5 Inch"
        case "AppleTV5,3": return "Apple TV"
        case "AppleTV6,2": return "Apple TV 4K"
        case "AudioAccessory1,1": return "HomePod"
        case "i386", "x86_64": return "Simulator"
        default: return identifier
        }
    }
}

extension UIButton {
    func appButton(title: String) {
        self.backgroundColor = .appThemeColor()
        self.addBorderAndCornerRadius(borderWidth: 1, borderColour: .white, cornerRadiusValue: 10)
        self.addShadow()
        let attributedTitle = NSAttributedString(string: title, attributes: [NSAttributedString.Key.font: UIFont.appSemiBoldFontWithSize(size: 20), NSAttributedString.Key.foregroundColor: UIColor.appBaseColor()])
        self.setAttributedTitle(attributedTitle, for: .normal)
    }
    func addBorderAndCornerRadius(borderWidth: CGFloat, borderColour: UIColor, cornerRadiusValue: CGFloat) {
        layer.cornerRadius = cornerRadiusValue
        layer.borderWidth = borderWidth
        layer.borderColor = borderColour.cgColor
    }
    func addCornerRadius(cornerRadiusValue: CGFloat) {
        layer.cornerRadius = cornerRadiusValue
    }
    func addBorder(borderWidth: CGFloat, borderColour: UIColor) {
        layer.borderWidth = borderWidth
        layer.borderColor = borderColour.cgColor
    }
}

extension UIView {
    func addViewBorderAndCornerRadius(borderWidth: CGFloat, borderColour: UIColor, cornerRadiusValue: CGFloat) {
        layer.cornerRadius = cornerRadiusValue
        layer.borderWidth = borderWidth
        layer.borderColor = borderColour.cgColor
    }
    func addViewCornerRadius(cornerRadiusValue: CGFloat) {
        layer.cornerRadius = cornerRadiusValue
    }
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    func addShadow(shadowOffset: CGSize, shadowRadius: CGFloat, shadowOpacity: Float) {
        layer.masksToBounds = false
        layer.shadowOffset = shadowOffset
        layer.shadowRadius = shadowRadius
        layer.shadowOpacity = shadowOpacity
    }
    func addShadow() {
        layer.masksToBounds = false
        layer.shadowOffset = CGSize(width: 0, height: 1.5)
        layer.shadowRadius = 2
        layer.shadowOpacity = 0.3
    }
    func addShadowMask() {
        layer.masksToBounds = true
        layer.shadowOffset = CGSize(width: 0, height: 1.5)
        layer.shadowRadius = 2
        layer.shadowOpacity = 0.3
    }
    //    func dropShadow(scale: Bool = true) {
    //        layer.masksToBounds = false
    //        layer.shadowColor = UIColor.black.cgColor
    //        layer.shadowOpacity = 0.5
    //        layer.shadowOffset = CGSize(width: -1, height: 1)
    //        layer.shadowRadius = 1
    //
    //        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
    //        layer.shouldRasterize = true
    //        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    //    }

    // OUTPUT 2
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius

        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}
extension UISearchBar {

    // Due to searchTextField property who available iOS 13 only, extend this property for iOS 13 previous version compatibility
    var compatibleSearchTextField: UITextField {
        guard #available(iOS 13.0, *) else { return legacySearchField }
        return self.searchTextField
    }

    private var legacySearchField: UITextField {
        if let textField = self.subviews.first?.subviews.last as? UITextField {
            // Xcode 11 previous environment
            return textField
        } else if let textField = self.value(forKey: "searchField") as? UITextField {
            // Xcode 11 run in iOS 13 previous devices
            return textField
        } else {
            // exception condition or error handler in here
            return UITextField()
        }
    }
}
