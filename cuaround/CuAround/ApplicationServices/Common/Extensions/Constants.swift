//
//  Constants.swift
//  CuAround
//
//  Created by Rizwan on 10/20/19.
//  Copyright © 2019 iOSApp. All rights reserved.
//

import Foundation
import Moya

class Constants {
    static let shared = Constants()
    
    let authProvider = MoyaProvider<AuthRequestManager>()
    let flowProvider = MoyaProvider<MainFlowRequestManager>()
    var appDarkMode = false
    
    var appModeKey: String {
        if appDarkMode {
            return "AppDarkTheme"
        }
        return "AppNormalTheme"
    }
}
