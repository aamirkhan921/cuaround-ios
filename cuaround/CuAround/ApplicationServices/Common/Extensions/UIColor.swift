//
//  UIColor.swift
//  CuAround
//
//  Created by Rizwan on 10/20/19.
//  Copyright © 2019 iOSApp. All rights reserved.
//

import UIKit
import ChameleonFramework

class AppThemeHandler: NSObject {
    static let shared = AppThemeHandler()
    
    var appTheming:NSDictionary? {
        return getAppTheming()
    }
    
    func getTargetName() -> String {
        //return Bundle.main.infoDictionary?["CFBundleName"] as! String
        return Constants.shared.appModeKey
    }
    
    func getAppTheming() -> NSDictionary {
        let themeFileName = getTargetName()
        
        if let path = Bundle.main.path(forResource: themeFileName, ofType: ".plist") {
            if let dict = NSDictionary(contentsOfFile: path) {
                return dict
            }
        }
        return NSDictionary()
    }
}

private let appTheme = AppThemeHandler.shared.appTheming

extension UIColor {
    class func appTransparencyBG() -> UIColor {
        if let colorCode = appTheme?.object(forKey: "appTransparencyColor") as? String {
            return UIColor(hexString: colorCode) ?? .clear
        }
        return .clear
    }
    
    class func appTextBg() -> UIColor {
        if let colorCode = appTheme?.object(forKey: "appTextBg") as? String {
            return UIColor(hexString: colorCode) ?? .clear
        }
        return .clear
    }
    
    class func appButtonBg() -> UIColor {
        if let colorCode = appTheme?.object(forKey: "appButtonBg") as? String {
            return UIColor(hexString: colorCode) ?? .clear
        }
        return .clear
    }
    
    class func appThemeColor() -> UIColor {
        if let colorCode = appTheme?.object(forKey: "appThemeColor") as? String {
            return UIColor(hexString: colorCode) ?? .white
        }
        return .black
    }
    
    class func appTintColor() -> UIColor {
        if let colorCode = appTheme?.object(forKey: "appTintColor") as? String {
            return UIColor(hexString: colorCode) ?? .clear
        }
        return .clear
    }
    
    class func screenBG() -> UIColor {
        if let colorCode = appTheme?.object(forKey: "screenBG") as? String {
            return UIColor(hexString: colorCode) ?? .clear
        }
        return .clear
    }
    
    class func appLightGreyColor() -> UIColor {
        if let colorCode = appTheme?.object(forKey: "appLightGreyColor") as? String {
            return UIColor(hexString: colorCode) ?? .clear
        }
        return .clear
    }
    
    class func appGreyTextColor() -> UIColor { // 150, 150, 150
        if let colorCode = appTheme?.object(forKey: "appGreyTextColor") as? String {
            return UIColor(hexString: colorCode) ?? .clear
        }
        return .clear
    }
    
    class func appThemeSelectedColor() -> UIColor { //255, 142, 49
        if let colorCode = appTheme?.object(forKey: "appThemeSelectedColor") as? String {
            return UIColor(hexString: colorCode) ?? .clear
        }
        return .clear
    }
    
    class func appThemeDisabledColor() -> UIColor {
        if let colorCode = appTheme?.object(forKey: "appThemeDisabledColor") as? String {
            return UIColor(hexString: colorCode) ?? .clear
        }
        return .clear
    }
    
    class func appTitleColor() -> UIColor {
        if let colorCode = appTheme?.object(forKey: "appTitleColor") as? String {
            return UIColor(hexString: colorCode) ?? .clear
        }
        return .clear
    }
    
    class func appBaseColor() -> UIColor {
        if let colorCode = appTheme?.object(forKey: "appBaseColor") as? String {
            return UIColor(hexString: colorCode) ?? .white
        }
        return .clear
    }
    
    class func appBodyColor() -> UIColor {
        if let colorCode = appTheme?.object(forKey: "appBodyColor") as? String {
            return UIColor(hexString: colorCode) ?? .clear
        }
        return .clear
    }
    
    class func appSubTitleColor() -> UIColor {
        if let colorCode = appTheme?.object(forKey: "appSubTitleColor") as? String {
            return UIColor(hexString: colorCode) ?? .clear
        }
        return .clear
    }
    
    // Main Colors
    
    class func appPlaceHolderColor() -> UIColor { //115, 118, 127
        if let colorCode = appTheme?.object(forKey: "appPlaceHolderColor") as? String {
            return UIColor(hexString: colorCode) ?? .clear
        }
        return .clear
    }
}

