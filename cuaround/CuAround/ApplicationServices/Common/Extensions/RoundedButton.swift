//
//  RoundedButton.swift
//  CuAround
//
//  Created by Rizwan on 10/20/19.
//  Copyright © 2019 iOSApp. All rights reserved.
//

import UIKit
import ChameleonFramework
@IBDesignable class RoundButton: UIButton {
    override init(frame: CGRect) {
           super.init(frame: frame)
           sharedInit()
       }
       
    @IBInspectable var cornerRadius: CGFloat = 15 {
        didSet {
            refreshCorners(value: cornerRadius)
        }
    }
       required init?(coder aDecoder: NSCoder) {
           super.init(coder: aDecoder)
           sharedInit()
       }
       
       override func prepareForInterfaceBuilder() {
           sharedInit()
       }
       
       func sharedInit() {
            self.backgroundColor = UIColor(hexString: "E8447E")
            self.setTitleColor(.white, for: .normal)
            refreshCorners(value: cornerRadius)
       }
    
    func refreshCorners(value: CGFloat) {
        layer.cornerRadius = value
    }
}

@IBDesignable class RoundTextField: UITextField {
    override init(frame: CGRect) {
           super.init(frame: frame)
           sharedInit()
       }
       
    @IBInspectable var cornerRadius: CGFloat = 20 {
        didSet {
            refreshCorners(value: cornerRadius)
            self.backgroundColor = .appTextBg()
            self.font = .appFontWithSize(size: 18)
        }
    }
       required init?(coder aDecoder: NSCoder) {
           super.init(coder: aDecoder)
           sharedInit()
       }
       
       override func prepareForInterfaceBuilder() {
           sharedInit()
       }
       
       func sharedInit() {
           refreshCorners(value: cornerRadius)
       }
    
    func refreshCorners(value: CGFloat) {
        layer.cornerRadius = value
        self.backgroundColor = .appTextBg()
        self.font = .appFontWithSize(size: 18)
    }
}


@IBDesignable

class VerticalButton: UIButton {
    @IBInspectable var padding: CGFloat = 8

    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()

        update()
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        update()
    }

    func update() {
        let imageBounds = self.imageView!.bounds
        let titleBounds = self.titleLabel!.bounds
        let totalHeight = imageBounds.height + padding + titleBounds.height

        self.imageEdgeInsets = UIEdgeInsets(
            top: -(totalHeight - imageBounds.height),
            left: 0,
            bottom: 0,
            right: -titleBounds.width
        )

        self.titleEdgeInsets = UIEdgeInsets(
            top: 0,
            left: -imageBounds.width,
            bottom: -(totalHeight - titleBounds.height),
            right: 0
        )
    }

     func intrinsicContentSize() -> CGSize {
        let imageBounds = self.imageView!.bounds
        let titleBounds = self.titleLabel!.bounds

        let width = imageBounds.width > titleBounds.width ? imageBounds.width : titleBounds.width
        let height = imageBounds.height + padding + titleBounds.height

        return CGSize(width: width, height: height)
    }
}
