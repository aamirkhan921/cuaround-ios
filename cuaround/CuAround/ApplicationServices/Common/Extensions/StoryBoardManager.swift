//
//  StoryBoardManager.swift
//  CuAround
//
//  Created by Rizwan on 10/24/19.
//  Copyright © 2019 iOSApp. All rights reserved.
//

import UIKit

enum AppStoryBoard : String {
    case Main = "Main"
    case PreLogin = "PreLogin"
    
    var instance : UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
    
    func viewController <T : UIViewController>(viewControllerClass: T.Type) -> T {
        let storyboardID = (viewControllerClass as UIViewController.Type).storyboardID
        return self.instance.instantiateViewController(withIdentifier: storyboardID) as! T
    }
    
    func initialViewController() -> UIViewController? {
        return instance.instantiateInitialViewController()
    }
}
extension UIViewController {
    class var storyboardID : String {
        return "\(self)"
    }
    
    static func initiateFromAppStoryBoard(appStoryBoard : AppStoryBoard) -> Self {
        return appStoryBoard.viewController(viewControllerClass: self)
    }
    
    
}
