//
//  UINavigationItem.swift
//  CuAround
//
//  Created by Rizwan on 10/31/19.
//  Copyright © 2019 iOSApp. All rights reserved.
//

import UIKit

let navigationTitleFontSize = 20.0
private let navBarBackIcon      =   "leftWhite"

extension UINavigationItem {
    
    func setCustomTitleView(title:String) {
        let mainTitle = UILabel()
        mainTitle.text = title
        mainTitle.font = UIFont.appSemiBoldFontWithSize(size: 25)
        mainTitle.sizeToFit()
        mainTitle.textColor = UIColor.appBaseColor()
        mainTitle.textAlignment = .right
        let mainTitleView = UIStackView(arrangedSubviews: [mainTitle])
        mainTitleView.distribution = .equalCentering
        mainTitleView.axis = .horizontal
        self.titleView?.backgroundColor = UIColor.appThemeColor()
        mainTitleView.frame = CGRect(x: 0, y: 0, width: mainTitle.frame.size.width, height: 35)
        self.titleView = mainTitleView
    }
    
    func setCustomTitleImageView(image:String) {
        let titleImage = UIImageView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        titleImage.contentMode = .scaleAspectFit
        titleImage.image = UIImage(named: image)
        self.titleView = titleImage
    }
    
    func setTitle(title:String, subtitle:String) {
        let mainTitle = UILabel()
        mainTitle.text = title
        mainTitle.font = UIFont.appSemiBoldFontWithSize(size: 25)
        mainTitle.sizeToFit()
        mainTitle.textColor = UIColor.appBaseColor()
        mainTitle.textAlignment = .center
        let subTitle = UILabel()
        subTitle.text = subtitle
        subTitle.font = UIFont.appSemiBoldFontWithSize(size: 15)
        subTitle.textAlignment = .center
        subTitle.sizeToFit()
        subTitle.textColor = UIColor.appThemeColor()
        subTitle.textAlignment = .center
        
        let navigationBarView = UIStackView(arrangedSubviews: [mainTitle, subTitle])
        navigationBarView.distribution = .equalCentering
        navigationBarView.axis = .vertical
        
        let width = max(mainTitle.frame.size.width, subTitle.frame.size.width)
        navigationBarView.frame = CGRect(x: 0, y: 0, width: width, height: 35)
        self.titleView = navigationBarView
    }
    func leftBackButton(btnTarget:AnyObject!, btnAction:Selector) {
        let button: UIButton = UIButton(type: .custom)
        button.setImage(UIImage(named: navBarBackIcon), for: UIControl.State.normal)
        button.frame = CGRect(x: 0, y: 0, width: 53, height: 31)//CGRectMake(0, 0, 53, 31)
        button.contentMode = .scaleAspectFit
        let barbuttonItem = UIBarButtonItem(image: UIImage(named: navBarBackIcon), style: .plain, target: btnTarget, action: btnAction)
        self.leftBarButtonItem = barbuttonItem
    }
}
