//
//  UIFont.swift
//  CuAround
//
//  Created by Rizwan on 10/20/19.
//  Copyright © 2019 iOSApp. All rights reserved.
//

import UIKit

extension UIFont {
    class func appFontWithSize(size: CGFloat) -> UIFont {
        return UIFont(name: "Avenir Next Condensed", size: size)!
    }
    
    class func appBoldFontWithSize(size: CGFloat) -> UIFont {
        return UIFont(name: "AvenirNextCondensed-Bold", size: size)!
    }
    
    class func appSemiBoldFontWithSize(size: CGFloat) -> UIFont {
        return UIFont(name: "AvenirNextCondensed-Medium", size: size)!
    }
    
    class func appDemiBoldFontWithSize(size: CGFloat) -> UIFont {
        return UIFont(name: "AvenirNextCondensed-DemiBold", size: size)!
    }
}

extension UILabel {
    func configureLabel(font: UIFont, textColor: UIColor, text: String) {
//        self.text = text
        self.font = font
        self.textColor = textColor
    }
}
