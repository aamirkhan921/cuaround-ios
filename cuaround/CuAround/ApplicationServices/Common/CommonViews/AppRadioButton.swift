//
//  AppRadioButton.swift
//  CuAround
//
//  Created by Rizwan on 11/5/19.
//  Copyright © 2019 iOSApp. All rights reserved.
//

import UIKit
class RadioButton: UIButton {
    var radioButtonIcon: UIButton?
    var radioButtonLabel: UILabel?
    var separatorView: UIView?
    
    var active: Bool {
        get {
            return radioButtonIcon?.isSelected ?? false
        } set {
            //getSeparator()
            let button = getButton()
            let label = getLabel()
            button.isSelected = newValue
            if newValue {
                label.textColor = UIColor.appBaseColor()
            } else {
                label.textColor = UIColor.appBaseColor()//appTransparencyBG()
            }
        }
    }
    func removeSeparator() {
        let separator = getSeparator()
        separator.removeFromSuperview()
    }
    
    var title: String {
        get {
            return getLabel().text ?? ""
        }
        set {
            let newLabel = getLabel()
            newLabel.text = newValue
        }
    }
    func getButton() -> UIButton {
        if radioButtonIcon == nil {
            let buttonIcon = UIButton(type: .custom)
            buttonIcon.translatesAutoresizingMaskIntoConstraints = false
            buttonIcon.isUserInteractionEnabled = false
            self.addSubview(buttonIcon)
            let activeImage = UIImage(named: "radioActive")
//            buttonIcon.setImage(activeImage, for: .selected)
            buttonIcon.setBackgroundImage(activeImage, for: .selected)
            let inactiveImage = UIImage(named: "radioInactive")
//            buttonIcon.setImage(inactiveImage, for: UIControl.State())
            buttonIcon.setBackgroundImage(inactiveImage, for: UIControl.State())
            //buttonIcon.tintColor = UIColor.appTransparencyBG()
            let bindings : [String: Any] = ["buttonIcon": buttonIcon]
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-(0)-[buttonIcon(==25)]",
                                                               options: NSLayoutConstraint.FormatOptions(),
                                                               metrics: nil,
                                                               views: bindings))
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-(0)-[buttonIcon(==25)]",
                                                               options: NSLayoutConstraint.FormatOptions(),
                                                               metrics: nil, views:
                bindings))
            self.addConstraint(NSLayoutConstraint(
                item: buttonIcon,
                attribute: NSLayoutConstraint.Attribute.centerY,
                relatedBy: NSLayoutConstraint.Relation.equal,
                toItem: self,
                attribute: NSLayoutConstraint.Attribute.centerY,
                multiplier: 1.0,
                constant: 0.0))
            radioButtonIcon = buttonIcon
        }
        return radioButtonIcon!
    }
    func getLabel() -> UILabel {
        if radioButtonLabel == nil {
            let buttonLabel = UILabel()
            buttonLabel.font = UIFont.appFontWithSize(size: 14)
            buttonLabel.textColor = .appBaseColor()
            buttonLabel.lineBreakMode = .byClipping
            buttonLabel.numberOfLines = 0
            buttonLabel.translatesAutoresizingMaskIntoConstraints = false
            buttonLabel.isUserInteractionEnabled = false
            self.addSubview(buttonLabel)
            let bindings = ["v": buttonLabel]
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-(0)-[v]-(0)-|",
                                                               options: NSLayoutConstraint.FormatOptions(),
                                                               metrics: nil,
                                                               views: bindings))
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-(35)-[v]-(0)-|",
                                                               options: NSLayoutConstraint.FormatOptions(),
                                                               metrics: nil,
                                                               views: bindings))
            self.addConstraint(NSLayoutConstraint(
                item: buttonLabel,
                attribute: NSLayoutConstraint.Attribute.centerY,
                relatedBy: NSLayoutConstraint.Relation.equal,
                toItem: self,
                attribute: NSLayoutConstraint.Attribute.centerY,
                multiplier: 1.0,
                constant: 0.0))
            radioButtonLabel = buttonLabel
        }
        return radioButtonLabel!
    }
    @discardableResult func getSeparator() -> UIView {
        if separatorView == nil {
            let separator = UIView()
            separator.translatesAutoresizingMaskIntoConstraints = false
            separator.backgroundColor = UIColor.white
            self.addSubview(separator)
            let bindings = ["v": separator]
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[v(==1.0)]-(0)-|",
                                                               options: NSLayoutConstraint.FormatOptions(),
                                                               metrics: nil,
                                                               views: bindings))
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-(20)-[v]-(0)-|",
                                                               options: NSLayoutConstraint.FormatOptions(),
                                                               metrics: nil,
                                                               views: bindings))
            separatorView = separator
        }
        
        return separatorView!
    }
}
