//
//  AppAlertView.swift
//  CuAround
//
//  Created by Rizwan on 10/20/19.
//  Copyright © 2019 iOSApp. All rights reserved.
//

import UIKit

class AppAlertView: NSObject {
    public static let sharedInstance = AppAlertView()
    private override init() {
    }
    
    func showAppAlert(vc:UIViewController, msg: String) {
        let actionSheetController:UIAlertController = UIAlertController(title: "", message: msg, preferredStyle: .alert)
        let cancelAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .cancel) { action ->
            Void in
        }
        actionSheetController.addAction(cancelAction)
        vc.present(actionSheetController, animated: true, completion: nil)
    }
    
    func showAppAlertWithTitle(vc:UIViewController, msg: String, title: String) {
        let actionSheetController:UIAlertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let cancelAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .cancel) { action ->
            Void in
        }
        actionSheetController.addAction(cancelAction)
        vc.present(actionSheetController, animated: true, completion: nil)
    }
}
