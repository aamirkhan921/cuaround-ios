//
//  LoadingViewIndicator.swift
//  CuAround
//
//  Created by Rizwan on 10/20/19.
//  Copyright © 2019 iOSApp. All rights reserved.
//

import Foundation
import NVActivityIndicatorView
class LoadingViewIndicator {
    var containerView = UIView()
    var loadingView = NVActivityIndicatorView(frame: CGRect(x: 0, y: 300, width: 80, height: 80), type: .ballBeat, color: .black, padding: 10.0)
    private static var instance = LoadingViewIndicator()
    
    public static let sharedInstance = LoadingViewIndicator()

    static func shared() -> LoadingViewIndicator {
        return instance
    }
    
    func startAnimating() {
        guard let window = sharedWindow() else { return }
        animateLoadingView(uiView: window)
//        vc.view.addSubview(loadingView)
//        loadingView.startAnimating()
//        loadingView.contentMode = .scaleAspectFit
    }
    func stopAnimating() {
        guard let window = sharedWindow() else { return }
        hideAnimateView(uiView: window)
    }
    private func sharedWindow() -> UIWindow? {
        let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        
        return window
    }
    func animateLoadingView(uiView: UIView) {
        uiView.isUserInteractionEnabled = false
        containerView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)//uiView.bounds
        
        containerView.backgroundColor = UIColor.white
        containerView.addShadow()
        containerView.layer.cornerRadius = 5
        containerView.clipsToBounds = true
        loadingView.center = containerView.center

        containerView.addSubview(loadingView)
        containerView.center = uiView.center
        uiView.addSubview(containerView)
        loadingView.contentMode = .scaleAspectFit
        loadingView.startAnimating()
    }
    func hideAnimateView(uiView: UIView) {
        loadingView.stopAnimating()
        containerView.removeFromSuperview()
        uiView.isUserInteractionEnabled = true
    }

}
