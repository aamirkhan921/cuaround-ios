//
//  CategoryMenuCell.swift
//  CuAround
//
//  Created by Rizwan on 11/18/19.
//  Copyright © 2019 iOSApp. All rights reserved.
//

import UIKit
import Kingfisher

class CategoryMenuCell: UICollectionViewCell {

    
    @IBOutlet weak var eventName:UILabel!
    @IBOutlet weak var eventDetail:UILabel!
    @IBOutlet weak var eventImage:UIImageView!
    @IBOutlet weak var eventImageType:UIImageView!
    
    @IBOutlet weak var borderView:UIView!
    @IBOutlet weak var cellView:UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
            configureUiViews()
    }

    func configureUiViews() {
        eventName.font = UIFont.appSemiBoldFontWithSize(size: 18)
        eventDetail.font = UIFont.appFontWithSize(size: 16)
        self.addViewCornerRadius(cornerRadiusValue: 10)
        self.addShadowMask()
    }
    
    func loadViews(objCatData:ObjectCategory) {
        eventName.text = objCatData.title
        eventDetail.text = objCatData.description
        let urlString = imageBaseUrl + objCatData.objectImage.replacingOccurrences(of: "\\", with: "/")
        let iconUrlString = imageBaseUrl + objCatData.objectIcon.replacingOccurrences(of: "\\", with: "/")
        eventImage?.kf.setImage(with: URL(string: urlString), placeholder: UIImage())
        eventImageType?.kf.setImage(with: URL(string: iconUrlString), placeholder: UIImage(named: "filter2Selected"))
        print(urlString)
    }

}
