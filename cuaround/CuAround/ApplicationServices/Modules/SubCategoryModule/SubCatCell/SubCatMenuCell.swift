//
//  SubCatMenuCell.swift
//  CuAround
//
//  Created by Rizwan on 12/17/19.
//  Copyright © 2019 iOSApp. All rights reserved.
//

import UIKit

class SubCatMenuCell: UICollectionViewCell {

    @IBOutlet weak var subCatName:UILabel!
    
    @IBOutlet weak var borderView:UIView!
    @IBOutlet weak var cellView:UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureUiViews()
    }

    func configureUiViews() {
        subCatName.font = UIFont.appSemiBoldFontWithSize(size: 18)
        self.addViewCornerRadius(cornerRadiusValue: 20)
        self.addShadowMask()
    }
    func loadViews(subCatData:SubCategory) {
        subCatName.text = subCatData.subCatName
        
    }
}
