//
//  ArticlesInteractor.swift
//  CuAround
//
//  Created by Rizwan on 1/30/20.
//  Copyright (c) 2020 iOSApp. All rights reserved.
//
//  This file was generated by the Clean Swift HELM Xcode Templates
//  https://github.com/HelmMobile/clean-swift-templates

import UIKit
import SwiftyJSON
protocol ArticlesInteractorInput {
    func getForumArticles(sortType: String, topicId: String)
    func commentArticles(articleId: String, description: String)
}

protocol ArticlesInteractorOutput {
    func fetchGetArticleResponse(articleData: [Article]?, errorMsg: String?)
    func fetchCommentArticleResponse(successMsg: String?, errorMsg: String?)
}

protocol ArticlesDataSource {
    
}

protocol ArticlesDataDestination {
    
}

class ArticlesInteractor: ArticlesInteractorInput, ArticlesDataSource, ArticlesDataDestination {
    
    var output: ArticlesInteractorOutput?
    
    // MARK: Business logic
    func commentArticles(articleId: String, description: String) {
        LoadingViewIndicator.sharedInstance.startAnimating()
        Constants.shared.flowProvider.request(.commentArticles(articleId, articleId, description)) { (result) in
           LoadingViewIndicator.sharedInstance.stopAnimating()
            switch result {
            case .success(let response):
            do {
                let json = try JSON(response.data)
                print(json)
                    let res = try JSONDecoder().decode(ApiResponse.self, from: response.data)
                    switch res.status {
                    case 400:
                        self.output?.fetchCommentArticleResponse(successMsg: nil, errorMsg: res.message)
                        break
                    case 200:
                        self.output?.fetchCommentArticleResponse(successMsg: res.message, errorMsg: nil)
                        break
                    default:
                        self.output?.fetchCommentArticleResponse(successMsg: nil, errorMsg: res.message)
                        break
                    }
                } catch {
                    self.output?.fetchCommentArticleResponse(successMsg: nil, errorMsg: "Something went wrong")
                    print(error.localizedDescription)
                }
            case .failure(let error):
                self.output?.fetchCommentArticleResponse(successMsg: nil, errorMsg: "Something went wrong")
                print(error)
            }
        }
    }
    func getForumArticles(sortType: String, topicId: String) {
        LoadingViewIndicator.sharedInstance.startAnimating()
        Constants.shared.flowProvider.request(.getArticles(sortType, topicId)) { (result) in
            LoadingViewIndicator.sharedInstance.stopAnimating()
            switch result {
            case .success(let response):
                do {
                    let json = try JSON(response.data)
                    print(json)
                    let res = try JSONDecoder().decode(ArticlesApiResponse.self, from: response.data)
                    switch res.status {
                    case 400:
                        self.output?.fetchGetArticleResponse(articleData: nil, errorMsg: res.message)
                        break
                    case 200:
                        self.output?.fetchGetArticleResponse(articleData: res.result, errorMsg: nil)
                        break
                    default:
                        self.output?.fetchGetArticleResponse(articleData: nil, errorMsg: res.message)
                        break
                    }
                } catch  {
                    print(error.localizedDescription)
                }
                
            case .failure(let error):
                print(error.errorDescription)
            }
        }
    }

}
