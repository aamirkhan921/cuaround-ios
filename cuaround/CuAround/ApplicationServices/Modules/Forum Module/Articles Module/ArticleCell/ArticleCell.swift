//
//  ArticleCell.swift
//  CuAround
//
//  Created by Rizwan on 2/2/20.
//  Copyright © 2020 iOSApp. All rights reserved.
//

import UIKit

class ArticleCell: UITableViewCell {

    @IBOutlet weak var articleTitle:UILabel!
    @IBOutlet weak var articleCommentsCount:UILabel!
    @IBOutlet weak var articleLikesCount:UILabel!
    @IBOutlet weak var articleCommentsButton:UIButton!
    @IBOutlet weak var articleDetailButton:UIButton!
    @IBOutlet weak var articleLikesButton:UIButton!
    @IBOutlet weak var articleCommentsImage:UIImageView!
    @IBOutlet weak var articleLikesImage:UIImageView!
    @IBOutlet weak var articleDescription:UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadCellViews() {
        
    }
}
