//
//  ArticlesPresenter.swift
//  CuAround
//
//  Created by Rizwan on 1/30/20.
//  Copyright (c) 2020 iOSApp. All rights reserved.
//
//  This file was generated by the Clean Swift HELM Xcode Templates
//  https://github.com/HelmMobile/clean-swift-templates

protocol ArticlesPresenterInput {
    func fetchGetArticleResponse(articleData: [Article]?, errorMsg: String?)
    func fetchCommentArticleResponse(successMsg: String?, errorMsg: String?)
}

protocol ArticlesPresenterOutput: class {
    func fetchGetArticleResponse(articleData: [Article]?, errorMsg: String?)
    func fetchCommentArticleResponse(successMsg: String?, errorMsg: String?)
}

class ArticlesPresenter: ArticlesPresenterInput {
    
    weak var output: ArticlesPresenterOutput?
    
    // MARK: Presentation logic
    func fetchGetArticleResponse(articleData: [Article]?, errorMsg: String?) {
        output?.fetchGetArticleResponse(articleData: articleData, errorMsg: errorMsg)
    }
    
    func fetchCommentArticleResponse(successMsg: String?, errorMsg: String?) {
        output?.fetchCommentArticleResponse(successMsg: successMsg, errorMsg: errorMsg)
    }
}
