//
//  ForumTopicViewController.swift
//  CuAround
//
//  Created by Rizwan on 1/21/20.
//  Copyright (c) 2020 iOSApp. All rights reserved.
//
//  This file was generated by the Clean Swift HELM Xcode Templates
//  https://github.com/HelmMobile/clean-swift-templates

import UIKit
import RSSelectionMenu

protocol ForumTopicViewControllerInput {
    func fetchGetTopicResponse(topicData: [TopicCategory]?, errorMsg: String?)
}

protocol ForumTopicViewControllerOutput {
    func getForumTopics(sortType: String)
}

class ForumTopicViewController: UIViewController, ForumTopicViewControllerInput {
    
    var output: ForumTopicViewControllerOutput?
    var router: ForumTopicRouter?
    
    @IBOutlet weak var topicsListTable: UITableView!
    @IBOutlet weak var sortTypeLabel: UILabel!
    var topicListArray = [TopicCategory]()
    
    var sortingArray = ["ASCENDING","DESCENDING"]
    var sortingSelectedArray = ["ASCENDING"]
    var sortingType: String = "ASCENDING" {
        didSet {
            sortTypeLabel.text = sortingType
            let orderBy = sortingType == "ASCENDING" ? "ASC" : "DEC"
            output?.getForumTopics(sortType: orderBy)
        }
    }
    // MARK: Object lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        ForumTopicConfigurator.sharedInstance.configure(viewController: self)
    }
    
    // MARK: View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let orderBy = sortingType == "ASCENDING" ? "ASC" : "DEC"
        output?.getForumTopics(sortType: orderBy)
    }
    // MARK: Requests
    func fetchGetTopicResponse(topicData: [TopicCategory]?, errorMsg: String?) {
        topicListArray.removeAll()
        if let topicDataArray = topicData {
            topicListArray = topicDataArray
            topicsListTable.reloadData()
        } else {
            
        }
    }
    
    @IBAction func addTopicPressed(sender: UIButton) {
        let addForumVc = AddForumViewController.initiateFromAppStoryBoard(appStoryBoard: .Main)
        addForumVc.isFromTopicVC = true
        router?.navigateToAddForumScreen(vc: addForumVc)
    }
    @IBAction func sortingPressed(sender: UIButton) {
        let selectionMenu =  RSSelectionMenu(selectionStyle: .single, dataSource: sortingArray, cellType: .custom(nibName: "AreaCell", cellIdentifier: "AreaCell")) { (cell, sortType, indexPath) in
            let customCell = cell as! AreaCell
            customCell.areaName.text = sortType
            customCell.areaName.textAlignment = .left
            customCell.tintColor = UIColor.appButtonBg()
        }
        selectionMenu.cellSelectionStyle = .checkbox
        
        selectionMenu.setSelectedItems(items: sortingSelectedArray) { [weak self] (sortType, index, selected, items) in
            self?.sortingType = sortType ?? ""
        }
        
        selectionMenu.tableView?.backgroundColor = .appTextBg()
        selectionMenu.showEmptyDataLabel()
        selectionMenu.uniquePropertyName = "sortingId"
        selectionMenu.onDismiss = { [weak self] selectedItems in
            // update selected items
            self?.sortingSelectedArray = selectedItems
            self?.sortingType = selectedItems.first ?? ""
        }
        selectionMenu.show(style: .alert(title: "Sort By", action: "Ok", height: 80), from: self)
    }
    // MARK: Display logic
    func configureTableView() {
        topicsListTable.register(UINib(nibName: "TopicCell", bundle: nil), forCellReuseIdentifier: "TopicCell")
    }
}

//This should be on configurator but for some reason storyboard doesn't detect ViewController's name if placed there
extension ForumTopicViewController: ForumTopicPresenterOutput {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        router?.passDataToNextScene(for: segue)
    }
}

extension ForumTopicViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let articleVC = ArticlesViewController.initiateFromAppStoryBoard(appStoryBoard: .Main)
        articleVC.topicDetail = topicListArray[indexPath.row]
        router?.navigateToArticleScreen(vc: articleVC)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return topicListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "TopicCell", for: indexPath) as? TopicCell {
            cell.loadViews(topicData: topicListArray[indexPath.row])
            return cell
        }
        return TopicCell()
    }
}
