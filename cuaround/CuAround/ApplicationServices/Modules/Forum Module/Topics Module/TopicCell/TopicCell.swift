//
//  TopicCell.swift
//  CuAround
//
//  Created by Rizwan on 1/21/20.
//  Copyright © 2020 iOSApp. All rights reserved.
//

import UIKit

class TopicCell: UITableViewCell {

    @IBOutlet weak var topicTitle:UILabel!
    @IBOutlet weak var topicArticleCount:UILabel!
    @IBOutlet weak var topicPostedDuration:UILabel!
    @IBOutlet weak var topicDescription:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureUIViews()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureUIViews() {
        topicTitle.font = UIFont.appSemiBoldFontWithSize(size: 20)
        topicDescription.font = UIFont.appFontWithSize(size: 16)
        topicArticleCount.font = UIFont.appBoldFontWithSize(size: 22)
        topicPostedDuration.font = UIFont.appSemiBoldFontWithSize(size: 14)
    }
    
    func loadViews(topicData:TopicCategory) {
        topicTitle.text = topicData.title
        topicArticleCount.text = "\(topicData.articleCount)"
        topicPostedDuration.text = topicData.postedTime
        topicDescription.text = topicData.description
    }
}
