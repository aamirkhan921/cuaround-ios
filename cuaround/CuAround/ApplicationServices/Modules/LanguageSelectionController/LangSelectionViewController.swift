//
//  LangSelectionViewController.swift
//  CuAround
//
//  Created by Rizwan on 10/22/19.
//  Copyright © 2019 iOSApp. All rights reserved.
//

import UIKit

class LangSelectionViewController: UIViewController {

    @IBOutlet weak var arabicButton:RoundButton!
    @IBOutlet weak var englishButton:RoundButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func switchLanguage(sender:RoundButton) {
        switch sender.tag {
        case 0:
            LocalizationManager.setAppLanguage(lang: "en")
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        case 1:
            LocalizationManager.setAppLanguage(lang: "ar")
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        default:
            break
        }
    }

}
