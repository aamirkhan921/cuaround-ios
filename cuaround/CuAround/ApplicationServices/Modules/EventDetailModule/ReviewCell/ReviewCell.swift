//
//  ReviewCell.swift
//  CuAround
//
//  Created by Rizwan on 1/13/20.
//  Copyright © 2020 iOSApp. All rights reserved.
//

import UIKit

class ReviewCell: UITableViewCell {

    
    @IBOutlet weak var reviewerName:UILabel!
    @IBOutlet weak var reviewBaseDetail:UILabel!
    @IBOutlet weak var reviewLabel:UILabel!
    @IBOutlet weak var ratingLabel:UILabel!
    @IBOutlet weak var dateLabel:UILabel!
    @IBOutlet weak var initialLabel:UILabel!
    @IBOutlet weak var helpFullButton:UIButton!
    
    @IBOutlet weak var ratingView:FloatRatingView!
    
    @IBOutlet weak var profileView:UIView!
    var helpButtonClosure : (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureUiViews()
    }

    @objc func helpfulButtonPressed(sender:UIButton) {
        helpButtonClosure?()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureUiViews() {
        reviewerName.font = UIFont.appSemiBoldFontWithSize(size: 16)
        reviewBaseDetail.font = UIFont.appFontWithSize(size: 14)
        reviewLabel.font = UIFont.appFontWithSize(size: 14)
        ratingLabel.font = UIFont.appFontWithSize(size: 14)
        dateLabel.font = UIFont.appFontWithSize(size: 16)
        initialLabel.font = UIFont.appFontWithSize(size: 17)
        
        profileView.addViewCornerRadius(cornerRadiusValue: profileView.frame.size.width/2)
        profileView.addShadow()
        helpFullButton.addTarget(self, action: #selector(helpfulButtonPressed(sender:)), for: .touchUpInside)
        helpFullButton.addBorderAndCornerRadius(borderWidth: 1.0, borderColour: .darkGray, cornerRadiusValue: 5)
    }
    
    func loadUiViews(reviewData:CustomerReview) {
        reviewerName.text = reviewData.userName
        initialLabel.text = "\(reviewData.userName?.first?.uppercased() ?? " ")"
        reviewBaseDetail.text = reviewData.description
        ratingView.rating = reviewData.rating
        ratingLabel.text = "\(Int(reviewData.rating)) ratings"
        reviewLabel.text = "\(reviewData.review) reviews"
        
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        //2019-12-03T01:52:38.74
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "MMM dd, yyyy"
        
        if let date = dateFormater.date(from: reviewData.date) {
            dateLabel.text = dateFormatterPrint.string(from: date)
        } else {
            dateFormater.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS"
            guard let date = dateFormater.date(from: reviewData.date) else {return}
            dateLabel.text = dateFormatterPrint.string(from: date)
        }
    }
}
