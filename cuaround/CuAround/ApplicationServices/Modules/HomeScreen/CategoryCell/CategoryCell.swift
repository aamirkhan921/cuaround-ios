//
//  CategoryCell.swift
//  CuAround
//
//  Created by Rizwan on 10/27/19.
//  Copyright © 2019 iOSApp. All rights reserved.
//

import UIKit
import ChameleonFramework

class CategoryCell: UICollectionViewCell {

    @IBOutlet weak var borderView:UIView!
    @IBOutlet weak var categoryName:UILabel!
    @IBOutlet weak var cellView:UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureUiViews()
    }
    
    func configureUiViews() {
        categoryName.font = UIFont.appSemiBoldFontWithSize(size: 20)
        self.addViewCornerRadius(cornerRadiusValue: 10)
        self.addShadowMask()
    }
    func loadViews(catData:Category) {
        categoryName.text = catData.catName
        borderView.addViewBorderAndCornerRadius(borderWidth: 1.0, borderColour: UIColor(hexString: catData.borderColor) ?? UIColor.appThemeColor(), cornerRadiusValue: 10)
        self.cellView.backgroundColor = UIColor(hexString: catData.bgColor) ?? UIColor.appThemeColor()
        categoryName.textColor = UIColor(hexString: catData.color) ?? UIColor.appThemeColor()
    }
}
