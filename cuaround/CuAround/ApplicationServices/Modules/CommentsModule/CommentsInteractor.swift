//
//  CommentsInteractor.swift
//  CuAround
//
//  Created by Rizwan on 4/26/20.
//  Copyright (c) 2020 iOSApp. All rights reserved.
//
//  This file was generated by the Clean Swift HELM Xcode Templates
//  https://github.com/HelmMobile/clean-swift-templates

protocol CommentsInteractorInput {
    
}

protocol CommentsInteractorOutput {
    
}

protocol CommentsDataSource {
    
}

protocol CommentsDataDestination {
    
}

class CommentsInteractor: CommentsInteractorInput, CommentsDataSource, CommentsDataDestination {
    
    var output: CommentsInteractorOutput?
    
    // MARK: Business logic
    

}
