//
//  CommentsConfigurator.swift
//  CuAround
//
//  Created by Rizwan on 4/26/20.
//  Copyright (c) 2020 iOSApp. All rights reserved.
//
//  This file was generated by the Clean Swift HELM Xcode Templates
//  https://github.com/HelmMobile/clean-swift-templates

import UIKit

// MARK: Connect View, Interactor, and Presenter

extension CommentsInteractor: CommentsViewControllerOutput, CommentsRouterDataSource, CommentsRouterDataDestination {
}

extension CommentsPresenter: CommentsInteractorOutput {
}

class CommentsConfigurator {
    // MARK: Object lifecycle
    
    static let sharedInstance = CommentsConfigurator()
    
    private init() {}
    
    // MARK: Configuration
    
    func configure(viewController: CommentsViewController) {
        
        let presenter = CommentsPresenter()
        presenter.output = viewController
        
        let interactor = CommentsInteractor()
        interactor.output = presenter
        
        let router = CommentsRouter(viewController: viewController, dataSource: interactor, dataDestination: interactor)
        
        viewController.output = interactor
        viewController.router = router
    }
}
