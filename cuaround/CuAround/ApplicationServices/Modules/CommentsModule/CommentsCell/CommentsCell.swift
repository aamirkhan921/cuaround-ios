//
//  CommentsCell.swift
//  CuAround
//
//  Created by Rizwan on 4/26/20.
//  Copyright © 2020 iOSApp. All rights reserved.
//

import UIKit

class CommentsCell: UITableViewCell {

    @IBOutlet weak var commentTitle:UILabel!
    @IBOutlet weak var commentDescription:UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
