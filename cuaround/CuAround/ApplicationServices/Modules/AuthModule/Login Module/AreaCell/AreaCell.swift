//
//  AreaCell.swift
//  CuAround
//
//  Created by Rizwan on 11/11/19.
//  Copyright © 2019 iOSApp. All rights reserved.
//

import UIKit

class AreaCell: UITableViewCell {

    @IBOutlet weak var areaName:UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        areaName.font = UIFont.appSemiBoldFontWithSize(size: 20)
        self.backgroundColor = .appTextBg()
        areaName.textColor = .appButtonBg()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
