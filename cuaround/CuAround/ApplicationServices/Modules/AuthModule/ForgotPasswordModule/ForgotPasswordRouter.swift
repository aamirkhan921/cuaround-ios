//
//  ForgotPasswordRouter.swift
//  CuAround
//
//  Created by Rizwan on 12/13/19.
//  Copyright (c) 2019 iOSApp. All rights reserved.
//
//  This file was generated by the Clean Swift HELM Xcode Templates
//  https://github.com/HelmMobile/clean-swift-templates

import UIKit

protocol ForgotPasswordRouterInput {
    
}

protocol ForgotPasswordRouterDataSource: class {
    
}

protocol ForgotPasswordRouterDataDestination: class {
    
}

class ForgotPasswordRouter: ForgotPasswordRouterInput {
    
    weak var viewController: ForgotPasswordViewController!
    weak private var dataSource: ForgotPasswordRouterDataSource!
    weak var dataDestination: ForgotPasswordRouterDataDestination!
    
    init(viewController: ForgotPasswordViewController, dataSource: ForgotPasswordRouterDataSource, dataDestination: ForgotPasswordRouterDataDestination) {
        self.viewController = viewController
        self.dataSource = dataSource
        self.dataDestination = dataDestination
    }
    
    // MARK: Navigation
    
    // MARK: Communication
    
    func passDataToNextScene(for segue: UIStoryboardSegue) {
        // NOTE: Teach the router which scenes it can communicate with
        
    }
}
