//
//  MapConfigurator.swift
//  CuAround
//
//  Created by Rizwan on 1/15/20.
//  Copyright (c) 2020 iOSApp. All rights reserved.
//
//  This file was generated by the Clean Swift HELM Xcode Templates
//  https://github.com/HelmMobile/clean-swift-templates

import UIKit

// MARK: Connect View, Interactor, and Presenter

extension MapInteractor: MapViewControllerOutput, MapRouterDataSource, MapRouterDataDestination {
}

extension MapPresenter: MapInteractorOutput {
}

class MapConfigurator {
    // MARK: Object lifecycle
    
    static let sharedInstance = MapConfigurator()
    
    private init() {}
    
    // MARK: Configuration
    
    func configure(viewController: MapViewController) {
        
        let presenter = MapPresenter()
        presenter.output = viewController
        
        let interactor = MapInteractor()
        interactor.output = presenter
        
        let router = MapRouter(viewController: viewController, dataSource: interactor, dataDestination: interactor)
        
        viewController.output = interactor
        viewController.router = router
    }
}
