//
//  NetworkManager.swift
//  CuAround
//
//  Created by Rizwan on 10/20/19.
//  Copyright © 2019 iOSApp. All rights reserved.
//

import Foundation
import Moya
let imageBaseUrl = "https://api.cuaround.app/"

protocol Networkable {
    var provider : MoyaProvider<AuthRequestManager> { get }
}

enum APIEnvironment {
    case staging
    case qa
    case production
}

struct NetworkManager {
    fileprivate let provider = MoyaProvider<AuthRequestManager>(plugins: [NetworkLoggerPlugin()])
    static let environment:APIEnvironment = .staging
}
