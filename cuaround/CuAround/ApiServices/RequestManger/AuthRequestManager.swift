//
//  AuthRequestManager.swift
//  CuAround
//
//  Created by Rizwan on 10/20/19.
//  Copyright © 2019 iOSApp. All rights reserved.
//

import Foundation
import Moya

public enum AuthRequestManager {

    case signIn(String, String)
    case signUp(String, String, String, String)
    case verifyMobile
    case forgotPassword(String)
    case resetPassword(String, String, String)
    case socialLogin(String, String, String, String)
}

extension AuthRequestManager: TargetType {
    var environmentBaseURL: String {
        switch NetworkManager.environment {
        case .staging:
            return appAlphaUrl
        case .qa:
            return appBetaUrl
        case .production:
            return appLiveUrl
        }
    }

    public var baseURL: URL {
        guard let url = URL(string: environmentBaseURL) else { fatalError("url not be formed") }
        return url
    }

    public var path: String {
        switch self {
        case .signIn:
            return "service/userLogin"
        case .signUp:
            return "service/userregister"
        case .verifyMobile:
            return "/verifyMobile"
        case .forgotPassword:
            return "service/forgotPassword"
        case .resetPassword:
            return "service/resetPassword"
        case .socialLogin:
            return "service/externalLogin"
        }
    }

    public var method: Moya.Method {
        switch self {
        case .signIn, .signUp, .verifyMobile, .forgotPassword, .resetPassword, .socialLogin:
            return .post
        }
    }

    public var sampleData: Data {
        return Data()
    }

    public var task: Task {
        switch self {
        case .signIn(let email, let password):
            let params = ["emailId": email, "password": password]
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
        case .signUp(let email, let password, let mobile, let name):
            let params = ["emailId": email, "password": password, "mobile": mobile, "name": name, "policy": "true"]
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
        case .socialLogin(let email, let userName, let deviceName, let provider):
            let params = ["EmailId": email, "UserName": userName, "DeviceName": deviceName, "Provider": provider]
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
        case .verifyMobile:
            return .requestParameters(parameters: [String: Any](), encoding: URLEncoding.default)
        case .forgotPassword(let emailOrMobile):
            let params = ["emailIdOrMobile": emailOrMobile]
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
        case .resetPassword(let emailOrMobile, let accessCode, let password):
            let params = ["emailIdOrMobile": emailOrMobile, "authCode": accessCode,
                "password": password]
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
        }
    }

    public var headers: [String: String]? {
        let headers = AppNetworkHeaders(requestName: self.path).headers
        return ["Content-Type": "application/x-www-form-urlencoded"]
    }

    public var validationType: ValidationType {
        return .successCodes
    }

}
