//
//  MainFlowRequestManager.swift
//  CuAround
//
//  Created by Rizwan on 10/31/19.
//  Copyright © 2019 iOSApp. All rights reserved.
//

import Foundation
import Moya

public enum MainFlowRequestManager {
    
    case getCategory
    case getSubCategory(String)
    case getObjectCategory(String, String)
    case getAddTopic(String, String)
    case getObjectCategorySubcategory(String, String)
    case getArea
    case getTopics(String)
    case contactUs(String, String)
    case getArticles(String, String)
    case getAddArticles(String, String, String)
    case likeArticle(String)
    case commentArticles(String, String, String)
}

extension MainFlowRequestManager : TargetType {
    var environmentBaseURL : String {
        switch NetworkManager.environment {
        case .staging:
            return appAlphaUrl
        case .qa:
            return appBetaUrl
        case .production:
            return appLiveUrl
        }
    }
    
    public var baseURL: URL {
        guard let url = URL(string: environmentBaseURL) else { fatalError("url not be formed") }
        return url
    }
    
    public var path: String {
        switch self {
        case .getCategory:
            return "category/getCategories"
        case .getObjectCategory:
            return "object/getObjectCategory"
        case .getSubCategory:
            return "subcategory/getSubCategories"
        case .getObjectCategorySubcategory:
            return "object/getObjectCategorySubCategory"
        case .getArea:
            return "area/getAreas"
        case .getTopics:
            return "topics/getTopics"
        case .getAddTopic:
            return "topics/addEditTopic"
        case .contactUs:
            return "service/contactUs"
        case .getArticles:
            return "article/getArticles"
        case .getAddArticles:
        return "article/addEditArticle"
        case .likeArticle:
            return "article/articleLike"
        case .commentArticles:
            return "article/articleComment"
        }
    }
    
    public var method: Moya.Method {
        switch self {
        case .getCategory,.getArea,.getSubCategory,.getObjectCategory,.getObjectCategorySubcategory,.getTopics,.getArticles:
            return .get
        case .contactUs,.getAddTopic, .getAddArticles,.likeArticle,.commentArticles:
            return .post
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        switch self {
        case .getCategory,.getArea:
            return .requestParameters(parameters: [String:Any](), encoding: URLEncoding.default)
        case .getArticles(let sortType, let topicId):
            let params = sortType.isEmpty ? [String:Any]() : ["orderBy":sortType, "topicId":topicId]
            print(params)
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
            case .getAddArticles(let articleTitle, let articleDescription, let topicId):
            let params = ["Title":articleTitle,"Description":articleDescription,"TopicId":topicId]
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
        case .commentArticles(let commentId, let articleId, let description):
            let params = ["ArticleId":articleId,"Description":description]//"CommentId":commentId,
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
        case .getTopics(let sortType):
            let params = sortType.isEmpty ? [String:Any]() : ["orderBy":sortType]
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
        case .likeArticle(let articleId):
            let params = ["ArticleId":articleId]
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
        case .getAddTopic(let topicTitle, let topicDescription):
            if let savedProfile = UserDefaults.standard.object(forKey: "UserProfileData") as? Data {
            if let userProfile = try? JSONDecoder().decode(UserProfile.self, from: savedProfile) {
                let params = ["Title":topicTitle,"Description":topicDescription,"UserId":userProfile.userId]
                return .requestParameters(parameters: params, encoding: URLEncoding.default)
                }
            } else {
                let params = ["Title":topicTitle,"Description":topicDescription,"UserId":""]
                return .requestParameters(parameters: params, encoding: URLEncoding.default)
            }
             return .requestParameters(parameters: [String:Any](), encoding: URLEncoding.default)
        case .getSubCategory(let areaId):
            let params = ["AreaId":areaId]
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
        case .getObjectCategory(let areaId, let categoryId),.getObjectCategorySubcategory(let areaId, let categoryId):
            let params = ["AreaId":areaId,"CategoryId":categoryId]
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
        case .contactUs(let email, let contactMessage):
        let params = ["emailid":email,"message":contactMessage]
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
        }
    }
    
    public var headers: [String : String]? {
//        let headers = AppNetworkHeaders(requestName: self.path).headers
        if let savedProfile = UserDefaults.standard.object(forKey: "UserProfileData") as? Data {
            if let userProfile = try? JSONDecoder().decode(UserProfile.self, from: savedProfile) {
                return ["Content-Type": "application/x-www-form-urlencoded",
                "Authorization": "Basic 2b5c62c8-46dd-4ed0-b14f-d14a6a98da21",
                "UserId": userProfile.userId]
            }
        }
        return ["Content-Type": "application/x-www-form-urlencoded",
                "Authorization": "Basic 2b5c62c8-46dd-4ed0-b14f-d14a6a98da21",
                "UserId": ""]
    }
    
    public var validationType: ValidationType {
        return .successCodes
    }

}
