//
//  NetworkHeaders.swift
//  CuAround
//
//  Created by Rizwan on 10/20/19.
//  Copyright © 2019 iOSApp. All rights reserved.
//

import Foundation
import UIKit

var authToken = ""
var appCurrentVersion = ""
var currentDateTime = Int()
protocol DictionaryConvertible {
    associatedtype KeyType: Hashable
    associatedtype ValueType
    var toDictionary: [KeyType: ValueType] { get }
}

protocol JSONStringConvertible {
    var toJSONString: String? { get }
}

protocol AppVersionSourceType {
    var appBundleVersion:String { get }
    var appBuildVersion:String { get }
}

internal struct AppNetworkHeaders {
    let requestName : String
    private let channel : Channel
    internal init(requestName: String) {
        self.requestName = requestName
        channel = Channel()
    }
    internal init(requestName: String, channel: Channel) {
        self.requestName = requestName
        self.channel = channel
    }
    internal var headers: [String: String] {
        return toDictionary
    }
    var channelString: String {
        return channel.toDictionary.toJSONString ?? String()
    }
    
}
extension AppNetworkHeaders: DictionaryConvertible {
    var toDictionary: [String: String] {
        return [
            "Content-Type": "application/x-www-form-urlencoded",
            "Device-Info": channelString
        ]
    }
}
struct AppBundle:AppVersionSourceType {
    let nsBundle = Bundle.main
    var appBundleVersion: String {
        return nsBundle.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
    }
    var appBuildVersion: String {
        return nsBundle.infoDictionary?["CFBundleVersion"] as? String ?? ""
    }
}
struct Channel {
    let buildVersion: String
    let bundleVersion: String
    let time: String
    init(versionSource: AppVersionSourceType = AppBundle(),
         time: String = NSDate().channelTimeStamp) {
        self.buildVersion = versionSource.appBuildVersion
        self.bundleVersion = versionSource.appBundleVersion
        appCurrentVersion = buildVersion
        self.time = time
        currentDateTime = (Int(NSDate().channelTimeStampWithSeconds))
    }
}
extension Channel: DictionaryConvertible {
    var toDictionary: [String: String] {
        return [
            "version": bundleVersion,
            "build_version": buildVersion,
            "client": "mobile-ios",
            "current_date": time,
            "device_name":UIDevice.current.modelName,
            "device_os":UIDevice.current.systemVersion
        ]
    }
}
extension Dictionary: JSONStringConvertible {
    var toJSONString: String? {
        guard let dictionaryData = data as Data? else { return nil }
        return NSString(data: dictionaryData, encoding: String.Encoding.utf8.rawValue) as String?
    }
    private var data: NSData? {
        let options = JSONSerialization.WritingOptions.prettyPrinted
        return try? JSONSerialization.data(withJSONObject: self, options: options) as NSData
    }
}
extension NSDate {
    func timeStamp(dateFormat format: String) -> String {
        let formatter:DateFormatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self as Date)
    }
}
private extension NSDate {
    var channelTimeStamp: String {
        return timeStamp(dateFormat: "yyyy-MM-dd HH:mm")
    }
    var channelTimeStampWithSeconds: Int64 {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
}
