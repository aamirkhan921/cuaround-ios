//
//  LocalizationManager.swift
//  CuAround
//
//  Created by Rizwan on 10/23/19.
//  Copyright © 2019 iOSApp. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
let languageKey = "AppleLanguages"//"AppLanguage"//

class LocalizationManager : NSObject {
    
    class func currentAppLanguage() -> String {
        let userDefults = UserDefaults.standard
        let languageArray = userDefults.object(forKey: languageKey) as? NSArray
        let current = languageArray?.firstObject as? String ?? String()
        if let language = current.components(separatedBy: "-").first {
            return language
        }
        return "en"
    }
    
    class func setAppLanguage(lang: String) {
        if lang == "en" {
            IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "Done"
            IQKeyboardManager.shared.toolbarNextBarButtonItemText = "Next"
        } else {
            IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "فعله"
            IQKeyboardManager.shared.toolbarNextBarButtonItemText = "التالى"
        }
        let userDefults = UserDefaults.standard
        userDefults.set([lang,currentAppLanguage()], forKey: languageKey)
        userDefults.synchronize()
    }
}

class Localizer: NSObject {
    class func DoTheSwizzling() {
        MethodSwizzleGivenClassName(cls: Bundle.self, originalSelector: #selector(Bundle.localizedString(forKey:value:table:)), overrideSelector: #selector(Bundle.specialLocalizedStringForKey(key:value:table:)))
    }
}
extension Bundle {
    @objc func specialLocalizedStringForKey(key: String, value: String?, table tableName: String?) -> String {
        /*2*/let currentLanguage = LocalizationManager.currentAppLanguage()
        var bundle = Bundle()
        /*3*/if let _path = Bundle.main.path(forResource: currentLanguage, ofType: "lproj") {
            bundle = Bundle(path: _path)!
        } else {
            let _path = Bundle.main.path(forResource: "Base", ofType: "lproj")!
            bundle = Bundle(path: _path)!
        }
        /*4*/return (bundle.specialLocalizedStringForKey(key: key, value: value, table: tableName))
    }
}

func MethodSwizzleGivenClassName(cls: AnyClass, originalSelector: Selector, overrideSelector: Selector) {
    let origMethod: Method = class_getInstanceMethod(cls, originalSelector)!;
    let overrideMethod: Method = class_getInstanceMethod(cls, overrideSelector)!;
    if (class_addMethod(cls, originalSelector, method_getImplementation(overrideMethod), method_getTypeEncoding(overrideMethod))) {
        class_replaceMethod(cls, overrideSelector, method_getImplementation(origMethod), method_getTypeEncoding(origMethod));
    } else {
        method_exchangeImplementations(origMethod, overrideMethod);
    }
}
